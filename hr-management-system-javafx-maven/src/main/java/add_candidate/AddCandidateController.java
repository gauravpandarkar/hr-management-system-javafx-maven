package add_candidate;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Scanner;

import add_vacancy_new.Veh;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import edit_candidate.EditCandidate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Callback;
import recruitment_dashboard.RecruitmentDashBoard;
import search_candidate.SearchCandidate;
import search_vacancy.SearchVacancy;

public class AddCandidateController implements Initializable {
	FileChooser fileChooser = new FileChooser();
	@FXML
	private TextField firstName;
	@FXML
	private TextField middleName;
	@FXML
	private TextField lastName;
	@FXML
	private ComboBox vacancy;
	@FXML
	private TextField email;
	@FXML
	private TextField contactNumber;
	@FXML
	private Button browse;
	@FXML
	private TextField keyword;
	@FXML
	private TextField date;
	@FXML
	private Button date1;
	@FXML
	private DatePicker applicationDate;
	@FXML
	private TextField note;
	@FXML
	private Button edit;
	@FXML
	private Button save;
	@FXML
	private Button search;
	@FXML
	private Button cancel;
	@FXML

	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancies;

	public void admin(ActionEvent event) {
		new AdminDashBoard().show();
	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();
	}

	public void candidate(ActionEvent event) {
		new SearchCandidate().show();
	}

	public void vacancies(ActionEvent event) {
		new SearchVacancy().show();

	}

	public void edit(ActionEvent event) {
		new EditCandidate().show();

	}
	public void cancel(ActionEvent event) {
		new SearchCandidate().show();

	}
    @FXML
	private TableView<Candidate> tableView;
	@FXML

	private TableColumn<Candidate, String> col1;
	@FXML
	private TableColumn<Candidate, String> col2;
	@FXML
	private TableColumn<Candidate, String> col3;
	@FXML
	private TableColumn<Candidate, String> col4;
	@FXML
	private TableColumn<Candidate, String> col5;
	@FXML
	private TableColumn<Candidate, String> col6;
	@FXML
	private TableColumn<Candidate, String> col7;
	@FXML
	private TableColumn<Candidate, String> col8;
	@FXML
	private TableColumn<Candidate, String> col9;
	

	private ObservableList<Candidate> data;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		ObservableList<String> list = FXCollections.observableArrayList("Associate IT Manager", "Junior Account Assistant", "Payroll Administrator","Sales Representative","Senior QA Lead","Senior Support Specialist","Software Engineer");
		vacancy.setItems(list);
		fileChooser.setInitialDirectory(new File("C:\\Users\\Gaurav\\OneDrive\\Documents"));
		col1.setCellValueFactory(new PropertyValueFactory<Candidate, String>("firstName"));
		col2.setCellValueFactory(new PropertyValueFactory<Candidate, String>("middleName"));
		col3.setCellValueFactory(new PropertyValueFactory<Candidate, String>("lastName"));

		col4.setCellValueFactory(new PropertyValueFactory<Candidate, String>("vacancy"));
		col5.setCellValueFactory(new PropertyValueFactory<Candidate, String>("email"));
		col6.setCellValueFactory(new PropertyValueFactory<Candidate, String>("contactNumber"));
		col7.setCellValueFactory(new PropertyValueFactory<Candidate, String>("keyword"));
		col8.setCellValueFactory(new PropertyValueFactory<Candidate, String>("date"));
		
//		this.col8.setCellFactory(new Callback<TableColumn<Candidate,Date>,TableCell<Candidate,Date>>){
//
//			@Override
//			public TableCell<Candidate, Date> call(TableColumn<Candidate, Date> arg0) {
//				// TODO Auto-generated method stub
//				return null;
//			}
//			
//			
//		});
		col9.setCellValueFactory(new PropertyValueFactory<Candidate, String>("note"));
		
		buildData();

	}

	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from requirement";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Candidate ca = new Candidate();
				ca.firstName.set(resultSet.getString(1));
				ca.middleName.set(resultSet.getString(2));
				ca.lastName.set(resultSet.getString(3));
				ca.vacancy.set(resultSet.getString(4));
				ca.email.set(resultSet.getString(5));
				ca.contactNumber.set(resultSet.getString(6));

				ca.keyword.set(resultSet.getString(7));

				ca.date.set(resultSet.getString(8));
				ca.note.set(resultSet.getString(9));

				data.add(ca);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void getText(MouseEvent event) {
		File file = fileChooser.showOpenDialog(new Stage());
	}

	public void save(ActionEvent event) {
		System.out.println(vacancy.getValue());
		System.out.println(firstName.getText());
		System.out.println(middleName.getText());
		System.out.println(lastName.getText());
		System.out.println(email.getText());
		System.out.println(contactNumber.getText());
		System.out.println(keyword.getText());
		System.out.println(note.getText());
		System.out.println(applicationDate.getValue());
		// System.out.println(browse.getText());

		String query = "insert into requirement(firstName,middleName,lastName,vacancy,Email,contactNumber,keyword,dateOfApplication,note"
				+ ") values ('" + firstName.getText() + "', '" + middleName.getText() + "','"
				+ lastName.getText() + "','" + vacancy.getValue() + "','" + email.getText()
				+ "','" + contactNumber.getText()+ "','" + keyword.getText()+ "','" + date.getText()+"','" + note.getText()
				+ "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur addcandidtae controller " + event.getEventType().getName());
		buildData();

		//new SearchCandidate().show();
	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();
	}

	public void browse(ActionEvent event) {

		File file = fileChooser.showOpenDialog(new Stage());
		// fileChooser.showSaveDialog(new Stage());
//		try {
//			Scanner sc=new Scanner(file);
//			while(sc.hasNextLine()) {
//				contactNumber.appendText(sc.nextLine()+ "\n");
//			}
//		}catch(FileNotFoundException e) {
//			e.printStackTrace();
//		}
	}
	public void date(ActionEvent event) {
		date.appendText(applicationDate.getValue().toString());
		
	}

}
