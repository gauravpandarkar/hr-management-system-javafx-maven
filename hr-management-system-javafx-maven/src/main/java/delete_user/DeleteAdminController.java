package delete_user;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import add.User;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class DeleteAdminController implements Initializable {
	@FXML
	private TextField userName;
	@FXML
	private Button cancel;
	@FXML
	private Button delete;
	@FXML
	private Button search;
	
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private TableView<User> tableView;
	@FXML

	private TableColumn<User, String> col1;
	@FXML
	private TableColumn<User, String> col2;
	@FXML
	private TableColumn<User, String> col3;
	@FXML
	private TableColumn<User, String> col4;
	@FXML
	private TableColumn<User, String> col5;
	@FXML
	private TableColumn<User, String> col6;
	private ObservableList<User> data;
	
	public void admin(ActionEvent event) {

		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {

	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();

	}

	public void cancel(ActionEvent event) {
		new AdminDashBoard().show();

	}
	public void delete(ActionEvent event) {
		System.out.println(userName.getText());

		String query = " delete from admin1 where userName ='" + userName.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
		
		buildData();

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		col1.setCellValueFactory(new PropertyValueFactory<User, String>("userRole"));
		col2.setCellValueFactory(new PropertyValueFactory<User, String>("employeeName"));
		col3.setCellValueFactory(new PropertyValueFactory<User, String>("status"));

		col4.setCellValueFactory(new PropertyValueFactory<User, String>("userName"));
		col5.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
		col6.setCellValueFactory(new PropertyValueFactory<User, String>("confirmPassword"));
		buildData();
		
	}
	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from admin1";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				User user = new User();
				user.userRole.set(resultSet.getString(2));
				user.employeeName.set(resultSet.getString(3));
				user.status.set(resultSet.getString(4));
				user.userName.set(resultSet.getString(5));
				user.password.set(resultSet.getString(6));
				user.confirmPassword.set(resultSet.getString(7));

				

				data.add(user);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	

}
