package delete_vacancy;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import add_vacancy_new.AddVacancy;
import add_vacancy_new.Veh;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment_dashboard.RecruitmentDashBoard;
import search_candidate.SearchCandidate;
import search_vacancy.SearchVacancy;

public class DeleteVacancyController implements Initializable {
	@FXML
	private TextField vacancyName;
	@FXML

	private Button candidate;
	@FXML
	private Button vacancies;
	@FXML
	private Button delete;
	@FXML
	private Button cancel;
	@FXML
	
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private TableView<Veh> tableView;
	@FXML
	
	private TableColumn<Veh, String> col1;
	@FXML
	private TableColumn<Veh, String> col2;
	@FXML
	private TableColumn<Veh, String> col3;
	@FXML
	private TableColumn<Veh, String> col4;
	@FXML
	private TableColumn<Veh, String> col5;
	private ObservableList<Veh> data;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	

		col1.setCellValueFactory(new PropertyValueFactory<Veh,String>("vacancyName"));
		col2.setCellValueFactory(new PropertyValueFactory<Veh,String>("description"));
		col3.setCellValueFactory(new PropertyValueFactory<Veh,String>("hiringManager"));
		col4.setCellValueFactory(new PropertyValueFactory<Veh,String>("noOfPositions"));
		col5.setCellValueFactory(new PropertyValueFactory<Veh,String>("jobTitle"));
		buildData();
		
	}
	public void buildData()
	{
		try {
			data=FXCollections.observableArrayList();
			String query = "Select*from vacancyadd";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Veh ve=new Veh();
				ve.vacancyName.set(resultSet.getString(2));
				ve.description.set(resultSet.getString(3));
				ve.hiringManager.set(resultSet.getString(4));
				ve.noOfPositions.set(resultSet.getString(5));
				ve.jobTitle.set(resultSet.getString(6));
				data.add(ve);
			}
			tableView.setItems(data);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	public void admin(ActionEvent event) {
		new AdminDashBoard().show();
	}
	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();
	}
	public void dashboard(ActionEvent event) {
		new DashBoard().show();
	}
	public void candidate(ActionEvent event) {
		new SearchCandidate().show();
	}
	public void vacancies(ActionEvent event) {
		new SearchVacancy().show();
	}
	public void delete(ActionEvent event) {
		System.out.println(vacancyName.getText());

		String query = " delete from vacancyadd where vacancyName ='" + vacancyName.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
		buildData();
		
		
		
		
	}
public void cancel(ActionEvent event) {
		
		new SearchVacancy().show();
		
	}
	

}
