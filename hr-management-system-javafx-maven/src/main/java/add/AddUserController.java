package add;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import add_candidate.Candidate;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment_dashboard.RecruitmentDashBoard;

public class AddUserController implements Initializable {
	@FXML
	private ComboBox userRole;
	@FXML
	private ComboBox status;
	@FXML
	private TextField employeeName;
	@FXML
	private TextField userName;
	@FXML
	private PasswordField password;
	@FXML
	private PasswordField confirmPassword;
	@FXML
	private Button save;
	@FXML
	private Button search;
	@FXML
	private Button cancel;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	 @FXML
		private TableView<User> tableView;
		@FXML

		private TableColumn<User, String> col1;
		@FXML
		private TableColumn<User, String> col2;
		@FXML
		private TableColumn<User, String> col3;
		@FXML
		private TableColumn<User, String> col4;
		@FXML
		private TableColumn<User, String> col5;
		@FXML
		private TableColumn<User, String> col6;
		
		

		private ObservableList<User> data;


	@FXML
	void Select(ActionEvent event) {
		String s = userRole.getSelectionModel().getSelectedItem().toString();

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		ObservableList<String> list = FXCollections.observableArrayList("Admin", "ESS");
		userRole.setItems(list);
		//userRole.getItems().add(list);
		ObservableList<String> list2 = FXCollections.observableArrayList("Enabled", "Disabled");
		status.setItems(list2);
		col1.setCellValueFactory(new PropertyValueFactory<User, String>("userRole"));
		col2.setCellValueFactory(new PropertyValueFactory<User, String>("employeeName"));
		col3.setCellValueFactory(new PropertyValueFactory<User, String>("status"));

		col4.setCellValueFactory(new PropertyValueFactory<User, String>("userName"));
		col5.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
		col6.setCellValueFactory(new PropertyValueFactory<User, String>("confirmPassword"));
		buildData();
	}
	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			
			String query = "Select*from admin1";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				User user = new User();
				user.userRole.set(resultSet.getString(2));
				user.employeeName.set(resultSet.getString(3));
				user.status.set(resultSet.getString(4));
				user.userName.set(resultSet.getString(5));
				user.password.set(resultSet.getString(6));
				user.confirmPassword.set(resultSet.getString(7));

				

				data.add(user);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public void admin(ActionEvent event) {

		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();

	}

	public void save(ActionEvent event) {
		System.out.println(userRole.getValue());
		System.out.println(status.getValue());
		System.out.println(employeeName.getText());
		System.out.println(userName.getText());
		System.out.println(password.getText());
		System.out.println(confirmPassword.getText());

		String query = "insert into admin1(userRole,status,employeeName,userName,password1,confirmPassword) values ('"
				+ userRole.getValue() + "', '" + status.getValue() + "','" + employeeName.getText() + "','"
				+ userName.getText() + "','" + password.getText() + "','" + confirmPassword.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());
		buildData();

		//new AdminDashBoard().show();

	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();

	}

	public void cancel(ActionEvent event) {
		new AdminDashBoard().show();

	}

}
