package recruitment_dashboard;

import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;
import search_candidate.SearchCandidate;
import search_vacancy.SearchVacancy;

public class RecruitmentDashBoardController {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancies;
	@FXML

	private Button dashboard;
	@FXML
	private Button cancel;
	@FXML
	private Button exit;

	

	
	
	

	public void admin(ActionEvent event) {

		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();

	}

	public void candidate(ActionEvent event) {
		new SearchCandidate().show();

	}

	public void vacancies(ActionEvent event) {
		new SearchVacancy().show();

	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();

	}

	public void cancel(ActionEvent event) {
		new DashBoard().show();

	}

	public void exit(ActionEvent event) {
		System.out.println("Exit Application");
		new Login().show();

	}
	

}
