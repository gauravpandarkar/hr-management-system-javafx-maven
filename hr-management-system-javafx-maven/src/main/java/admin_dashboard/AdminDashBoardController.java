package admin_dashboard;

import add.AddUser;
import dashboard.DashBoard;
import delete_user.DeleteAdmin;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import login.Login;
import recruitment_dashboard.RecruitmentDashBoard;
import search_admin.SearchAdmin;

public class AdminDashBoardController {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button add;
	@FXML
	private Button delete;
	@FXML
	private Button search1;
	@FXML
	private Button dashboard;
	@FXML
	private Button cancel;
	@FXML
	private Button exit;
	@FXML
	public void background(MouseEvent event) {
		Button button=new Button("admin");
		button.setStyle("-fx-background-color:#ff0000;");
	}
	

	public void admin(ActionEvent event) {
		Button button=new Button("admin");
		button.setStyle("-fx-background-color:#ff0000;");
		
		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();

	}

	public void add(ActionEvent event) {
		new AddUser().show();

	}

	public void search(ActionEvent event) {
		new SearchAdmin().show();

	}

	public void delete(ActionEvent event) {
		new DeleteAdmin().show();

	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();

	}

	public void cancel(ActionEvent event) {
		new DashBoard().show();

	}

	public void exit(ActionEvent event) {
		new Login().show();
		System.out.println("Exit Application");

	}

}
