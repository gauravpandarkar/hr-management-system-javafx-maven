package delete_candidate;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import add_candidate.AddCandidate;
import add_candidate.Candidate;
import admin_dashboard.AdminDashBoard;
import dashboard.DashBoard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment_dashboard.RecruitmentDashBoard;
import search_candidate.SearchCandidate;
import search_vacancy.SearchVacancy;

public class DeleteCandidateController implements Initializable{
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancies;
	@FXML

	private Button dashboard;
	@FXML
	private Button cancel;
	@FXML
	private Button delete;
	
	@FXML
	private TextField candidateToDelete;
	
	 @FXML
		private TableView<Candidate> tableView;
		@FXML

		private TableColumn<Candidate, String> col1;
		@FXML
		private TableColumn<Candidate, String> col2;
		@FXML
		private TableColumn<Candidate, String> col3;
		@FXML
		private TableColumn<Candidate, String> col4;
		@FXML
		private TableColumn<Candidate, String> col5;
		@FXML
		private TableColumn<Candidate, String> col6;
		@FXML
		private TableColumn<Candidate, String> col7;
		@FXML
		private TableColumn<Candidate, String> col8;
		@FXML
		private TableColumn<Candidate, String> col9;
		

		private ObservableList<Candidate> data;


	@Override
	public void initialize(URL url, ResourceBundle rb) {

		data=FXCollections.observableArrayList();

		
		col1.setCellValueFactory(new PropertyValueFactory<Candidate, String>("firstName"));
		col2.setCellValueFactory(new PropertyValueFactory<Candidate, String>("middleName"));
		col3.setCellValueFactory(new PropertyValueFactory<Candidate, String>("lastName"));

		col4.setCellValueFactory(new PropertyValueFactory<Candidate, String>("vacancy"));
		col5.setCellValueFactory(new PropertyValueFactory<Candidate, String>("email"));
		col6.setCellValueFactory(new PropertyValueFactory<Candidate, String>("contactNumber"));
		col7.setCellValueFactory(new PropertyValueFactory<Candidate, String>("keyword"));
		//col8.setCellValueFactory(new PropertyValueFactory<Candidate, String>("applicationDate"));

		col9.setCellValueFactory(new PropertyValueFactory<Candidate, String>("note"));
	buildData();
	}
		
	public void buildData()
	{
		try {
			data=FXCollections.observableArrayList();
			String query = "Select*from requirement";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Candidate ca=new Candidate();
				ca.firstName.set(resultSet.getString(1));
				ca.middleName.set(resultSet.getString(2));
				ca.lastName.set(resultSet.getString(3));
				ca.vacancy.set(resultSet.getString(4));
				ca.email.set(resultSet.getString(5));
				ca.contactNumber.set(resultSet.getString(6));

				ca.keyword.set(resultSet.getString(7));

				ca.dateOfApplication.set(resultSet.getString(8));

				ca.note.set(resultSet.getString(9));

				data.add(ca);
			}
			tableView.setItems(data);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	

	public void admin(ActionEvent event) {

		new AdminDashBoard().show();

	}

	public void recruitment(ActionEvent event) {
		new RecruitmentDashBoard().show();

	}

	public void candidate(ActionEvent event) {
		new SearchCandidate().show();

	}

	public void vacancies(ActionEvent event) {
		new SearchVacancy().show();

	}

	public void dashboard(ActionEvent event) {
		new DashBoard().show();

	}
	public void cancel(ActionEvent event) {
		new SearchCandidate().show();

	}



	public void delete(ActionEvent event) {
		System.out.println(candidateToDelete.getText());

		String query = " delete from requirement where firstName ='" + candidateToDelete.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
		buildData();
		

	}
		

}
