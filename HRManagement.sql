CREATE DATABASE company;
use company;
create table requirement(
firstName varchar(50),
middleName varchar(50),
lastName varchar(50),
vacancy varchar(60),
Email varchar(60),
contactNumber varchar(60),
keyword varchar(50),
dateOfApplication varchar(60),
note varchar(50)

);
create table admin1(
id int auto_increment,
userRole varchar(50),
employeeName varchar(60),
status varchar(60),
userName varchar(50),
password1 varchar(50),
confirmPassword varchar(50),
PRIMARY KEY (id)
);
create table user(
id int auto_increment,
userName varchar(60),
password varchar(70),
PRIMARY KEY(id)
);
create table vacancyadd(
id int auto_increment,
vacancyName varchar(70),
jobTitle varchar(50),
description1 varchar(80),
hiringManager varchar(60),
numberOfPositions varchar(60),
PRIMARY KEY(id)
);
drop table requirement;
drop table admin1;

select * from requirement;
drop table requirement; 
select*from user;
select * from vacancyadd;